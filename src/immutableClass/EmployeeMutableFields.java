/*
 * Ref Links:
 * https://www.journaldev.com/129/how-to-create-immutable-class-in-java
 * https://stackoverflow.com/questions/12451013/immutable-objects-and-unmodifiable-collections
 * https://stackoverflow.com/questions/34109363/how-can-we-maintain-immutability-of-a-class-with-a-mutable-reference
 * https://howtodoinjava.com/java/basics/how-to-make-a-java-class-immutable
 * https://stackoverflow.com/questions/29740709/example-of-an-immutable-class-with-hashmap
 */
package immutableClass;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/*
 * Create a Immutable Class containing below mentioned Instance Variables.
 * String name
 * List<Skills> skills
 * Date joiningDate
 * Map<String, String> nameSkill
 */
public class EmployeeMutableFields {
	private final String name;
	/**
	    * List is mutable object as it provide setters to change various date/time parts
	  **/
	private final List<String> skills;
	 /**
	    * Date class is mutable as it provide setters to change various date/time parts
	  **/
	private final Date joiningDate;
	private final Map<String, String> nameSkill;
	
	private EmployeeMutableFields(String name, List<String> skills, Date joiningDate, HashMap<String, String> nameSkill) {
		
		//String is an Immutable Object.
		this.name = name;
		/*
		 * Since List/ Collection is Mutable object.
		 * Hence It can be made Read Only by using unmodifiableList() for List.
		 */
		this.skills = Collections.unmodifiableList(new ArrayList<String>(skills));
		/*
		 * Since, Date is a Mutable Object.
		 * Hence in the constructor.
		 * A new Object should be created
		 */
		this.joiningDate = new Date(joiningDate.getTime());
		
		/*
		 * Since Map is Mutable object.
		 * We need to create a New HashMap Object.
		 * Approach 1:
		 * We can create a Read Only view of Map using unmodifiableMap method of Collections class.
		 * Store that into the nameSkill instance Map object.
		 * 
		 * Approach 2:
		 * Iterate the Entry Object inside the HashMap Object.
		 * And put all the Key-Value pair inside the instance Variable of HashMap object.
		 */
		//Approach 1:
		Map<String,String> map = new HashMap<String,String>();
	    map.putAll(nameSkill);
	    this.nameSkill = Collections.unmodifiableMap(map);
	    
	    //Approach 2:
		/*this.nameSkill = new HashMap<String, String>();
	    for (Entry<String, String> entry: nameSkill.entrySet()) {
	      this.nameSkill.put((entry.getKey()), entry.getValue());
	    }*/
	}
	
	//Factory method to store object creation logic in single place
	public static EmployeeMutableFields getInstance(String name, List<String> skills, Date joiningDate, HashMap<String, String> nameSkill) {
		return new EmployeeMutableFields(name, skills, joiningDate, nameSkill);
	}
	
	/*
	 * We should not provide any Setter Method.
	 * Getters Can be provided using below approach.
	 */
	
	/**
	    * Integer class is immutable so we can return the instance variable as it is
	 **/
	public String getName() {
		return this.name;
	}
	
	/**
	    * List is mutable object so we need a little care here.
	    * We should not return the reference of original instance variable.
	    * Instead we create the unmodifiableList using the Collections.unmodifiableList(List l).
	    * So that it is made Read Only.
	  **/
	public List<String> getSkills() {
        return Collections.unmodifiableList(this.skills);
    }
	
	/**
	    * Date class is mutable so we need a little care here.
	    * We should not return the reference of original instance variable.
	    * Instead a new Date object, with content copied to it, should be returned.
	  **/
	public Date getJoiningDate() {
		return new Date(this.joiningDate.getTime());
	}
	
  /**
    * Map is mutable object so we need a little care here.
    * We should not return the reference of original instance variable.
    * Instead we create the unmodifiableMap using the Collections.unmodifiableMap(Map m).
    * So that it is made Read Only.
  **/
	public Map<String, String> getNameSkill() {
	    return Collections.unmodifiableMap(this.nameSkill);
	}

	@Override
	public String toString() {
		return "EmployeeMutableFields [name=" + name + ", skills=" + skills + ", joiningDate=" + joiningDate + "]";
	}
}
