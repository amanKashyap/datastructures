package customArrayList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyArrayList {
	
	//Used to internally store elements as Array
	private Object[] dataStore;
	//used to store current size of arrayList
	private int size;
	
	/*
	 * modCount keeps the info that how many times this arrayList class has been modified.
	 * i.e. whenever an element has been added, removed etc. structural changes.
	 * Because of the use of modCount the Iterator becomes Fail Fast Iterator.
	 */
	private int modCount = 0;
	
	public MyArrayList() {
		//initial size of array.
		dataStore = new Object[10];
	}
	
	//add operation
	public boolean add(Object o) {
		
		ArrayList<String> al = new ArrayList<>();
		/*
		 * ArrayList is a resizable array. 
		 * Hence, we need to check whether we need to increase the capacity of arrayList or not.
		 * i.e. increase the size when capacity gets full or reaches a Threshold.
		 */
		if(dataStore.length-size <= 0) {
			//i.e. Almost Full.
			increaseCapacity();
		}
		dataStore[size] = o;
		size++;
		//Incremented whenever an element is added
		modCount++;
		return true;
	}
	
	public void increaseCapacity() {
		/*
		 * Using Arrays.copyOf(original, newLength) which copies the original array i.e dataStore
		 * into a new array of specified length i.e. newLength.
		 * We would be incrementing length by 10.
		 */
		dataStore = Arrays.copyOf(dataStore, dataStore.length+10);
	}
	
	
	
	//get Operation
	public Object get(int index) {
		//We should validate the index. Whether it is correct or not.
		checkRange(index);
		return dataStore[index];
	}
	
	//checkRange() would validate the index, whether it is a valid index or not.
	public void checkRange(int index) {
		if(index > size) {
			throw new IndexOutOfBoundsException();
		}
	}
	
	
	//remove Operation
	public Object remove(int index) {
		//Validate whether the index is valid or not.
		checkRange(index);
		//Store the element that we have to delete.
		Object removedValue = dataStore[index];
		/*
		* After Deleting we need to restructure the array.
		* Shift all the elements are the deleted element to the left.
		*/ 
		//Number of elements to be moved:
		int dataMov = size-index-1;
		/*
		 * Copies the source array starting from the sourceIndex into the destination array 
		 * starting from destination index and dataMoved no of elements are shifted.
		 * System.arraycopy(sourceArray, SourceIndex, DestinationArray, 
		 * 					destinationIndex, NumOfElementsToBeMoved);
		 */
		
		System.arraycopy(dataStore, index+1, dataStore, index, dataMov);
		size--;
		/*
		 * Basically above mentioned operation shifts the elements by 1 to the left.
		 * Hence, we need to nullify the last element of array
		 */
		dataStore[size] = null;
		//Incremented Whenever an Element is removed
		modCount++;
		return removedValue;
	}
	
	
	
	//Contains Operation
	public boolean contains(Object o) {
		/*
		 * Iterate all the elements of the arrayList.
		 * Check each element with the passed object.
		 * If found return true.
		 * Else return false.
		 */
		for(int i = 0; i < size; i++) {
			if(o.equals(dataStore[i])) {
				return true;
			}
		}
		return false;
	}
	
	//Creating a custom Iterator of our ArrayList class
	private class Itr<E> implements Iterator<E> {
		/*
		 * Iterator returned by ArrayList class is a FailFast Iterator.
		 * On detecting concurrentModification over collection on which it is iterating fails.
		 * Throws an exception ConcurrentModificationException.
		 */
		/*
		 * next -> points to the next element returned by the iterator.
		 * previous -> points to the previous elements that is returned by this iterator.
		 * default value of previous is -1 which indicates that there is no such previous element.
		 */
		int next;
		int previous = -1;
		/*
		 * Used to verify the expectedModCount (modCount at the time of creation of Iterator.)
		 * with modCount while next() or remove() method is called to basically
		 * ensure that the ArrayList has not been structurally modified while iteration is going on.
		 */
		int expectedModCount = modCount;
		
		@Override
		public boolean hasNext() {
			/*
			 * Checks is next is equal to size or not.
			 * If it is not equal to size means there are some elements left.
			 * Because we increment the next var only when the next element is accessed.
			 * When the whole list is travessed the next will get equal to size.
			 * In which case the below condition would return false.
			 */
			return next!=size;
		}
		
		/*
		 * @see java.util.Iterator#next()
		 * Basically we would keep traversing next() to Iterate the 
		 * list until the size of list is reached.
		 */
		@Override
		public E next() {
			checkForComodification();
			int i = next;
			if(i >= size) {
				throw new NoSuchElementException();
			}
			/*
			 * Maintained a local copy of data Stored.
			 */
			Object[] listArray = MyArrayList.this.dataStore;
			next = i+1;
			previous = i;
			return (E) listArray[previous];
		}
		
		@Override
		public void remove() {
			checkForComodification();
			MyArrayList.this.remove(previous);
			next = previous;
			previous = -1;
		}
		
		final void checkForComodification() {
			/*
			 * Throws an exception if somebody has modified the arrayList while the 
			 * iteration was going on.
			 */
			if(modCount != expectedModCount) {
				throw new ConcurrentModificationException();
			}
		}
	}
	
}
