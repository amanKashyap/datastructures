package customDoublyLinkedList;

public class Node {
	private int data;
	public Node previous;
	public Node next;
	
	public Node(int data) {
		this.data = data;
		this.previous = null;
		this.next = null;
	}
	
	public Node(Node previous, int data, Node next) {
		this.previous = previous;
		this.data = data;
		this.next = next;
	}
	
	public int getData() {
		return this.data;
	}
}
