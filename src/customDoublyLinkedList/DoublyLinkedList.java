package customDoublyLinkedList;

public class DoublyLinkedList {
	private Node head;
	private int size;

	public DoublyLinkedList() {
		this.head = null;
		this.size = 0;
	}

	public boolean isEmpty() {
		return head == null;
	}

	public int size() {

		return size;
	}

	public void printNode() {
		Node currentNode = head;
		while (currentNode != null) {
			System.out.println(currentNode.getData());
			currentNode = currentNode.next;
		}
	}

	/*
	 * Given the int data. Add a New Node to the beginning of the Linked List.
	 */
	public void insertAtBegin(int data) {
		/*
		 * If DLL is empty. Create a new Node, name it Head. firstNode i.e. head = |
		 * null | data | null |
		 * 
		 * Check for the Boundary Condition.
		 */
		if (head == null) {
			head = new Node(data);
		}
		/*
		 * If the DLL is not empty. There are some nodes already present and we need to
		 * add a newNode at the beginning of the Linked List. Create a New Node : | null
		 * | data | head | Points to head as Head would be the next Node. Update the
		 * previous pointer from null to newNode. Update the Head pointer.
		 */
		else {
			Node newNode = new Node(null, data, head);
			head.previous = newNode;
			head = newNode;
		}
		size++;
	}

	/*
	 * Given the int data. Add a New Node to the end of the Linked List.
	 */
	public void insertAtEnd(int data) {
		/*
		 * If DLL is empty. Create a new Node, name it Head. firstNode i.e. head = |
		 * null | data | null |
		 * 
		 * Check for the Boundary Condition.
		 */
		if (head == null) {
			head = new Node(data);
		} else {
			Node currentNode = head;
			/*
			 * Iterate the List till the end. At the end of while loop, currentNode would be
			 * referencing to the Last Node of the List.
			 */
			while (currentNode.next != null) {
				currentNode = currentNode.next;
			}

			/*
			 * Create a New Node. Add the previous Node link as currrentNode, which
			 * references the last node of Linked List.
			 */
			Node newNode = new Node(currentNode, data, null);
			/*
			 * Create the next pointer the connection between the currentNode and the
			 * newNode.
			 */
			currentNode.next = newNode;
			size++;
		}
	}
	
	/*
	 * Remove the First Element from the Doubly Linked List.
	 */
	public Node removeFirst() {
		if(head == null) {
			System.out.println("List is Empty");
			//Usually an exception would be thrown.
			return head;
		} else {
			Node current = head;
			head = head.next;
			head.previous = null;
			size--;
			
			return current;
		}
	}

	
	public Node removeEndApproach1() {
		//Approach 1:
		 if(head == null) {
			System.out.println("List is Empty");
			return head;
		} 
		
		Node last = head;
		Node previousToLast = null;
		
		while(last.next != null) {
			previousToLast = last;
			last = last.next;
		}
		if(previousToLast != null) {
			previousToLast.next = null;
		}
		size--;
		return last;
	}
	
	public Node removeEndApproach2() {
			//Approach 2:
			if(head == null) {
				System.out.println("List is Empty");
				return head;
			}
			
			Node current = head;
			if(head.next == null) {
				head = null;
				size--;
				
				return current;
			}
			
			while(current.next.next != null) {
				current = current.next;
			}
			current.next = null;
			size--;
			return current;
	}
	
	
	public void insertAt(int data, int index) {
		//Check Boundary Condition
		if(head == null) {
			System.out.println("Linked List is Empty");
			return;
		}
		
		/*
		 * Check if Index is Valid or not.
		 * If Index 0 or Negative - Invalid
		 * If Index > Size - Invalid.
		 */
		if(index < 1 || index > size) {
			System.out.println("Index is Invalid");
			return;
		}
		
		Node currentNode = head;
		int count = 1;
		while(count < index) {
			currentNode = currentNode.next;
			count++;
		}
		
		/*
		 * If the Index Points to the First Node.
		 */
		if(currentNode.previous == null) {
			Node newNode = new Node(null, data, currentNode);
			currentNode.previous = newNode;
			head = newNode;
		} else {
			Node newNode = new Node(currentNode.previous, data, currentNode);
			currentNode.previous.next = newNode;
			currentNode.previous = newNode;
		}
		
		size++;
	}
	
	
	public void removeAt(int index) {
		/*
		 * Check Boundary Condition.
		 * Check if Linked List is Empty or not.
		 */
		if(head == null) {
			System.out.println("Linked List is Empty");
			return;
		}
		/*
		 * Check Boundary Condition.
		 * Check if index passed to the method is valid or not.
		 */
		if(index < 1 || index > size) {
			System.out.println("Index is Invalid");
			throw new IllegalArgumentException("Index is Invalid");
		}
		
		Node currentNode = head;
		int i = 1;
		
		/*
		 * Iterate to the Node present at the mentioned Index.
		 */
		while(i < index) {
			currentNode = currentNode.next;
			i++;
		}
		
		/*
		 * If the Node present at the mentioned index is the Last Node.
		 * eg:-> <A>|null|1|B| --> <B>|A|2|C| --> <C>|B|3|null| (currentNode)
		 * 		currentNode.previous --> <B>|A|2|C|
		 * 		currentNode.previous.next = C
		 * 		(Making that null would disconnect the Node C from the Linked List.
		 * 			Making the Node C eligible for Garbage Collection)
		 */
		if(currentNode.next == null) {
			currentNode.previous.next = null;
		}
		/*
		 * If the Node present at the mentioned index is the First Node.
		 * eg:-> (currentNode)<Head> <A> |null|1|B| --> <B> |A|2|C| --> <C> |B|3|null| 
		 * ----> currentNode = currentNode.next i.e. (currentNode) <B> |A|2|C|
		 * ----> currentNode.previous i.e. A
		 * ----> head = currentNode
		 * (Making currentNode.previous as null and moving the head pointer,
		 * Would disconnect Node A from the Linked List.
		 * Hence, Eligible for Garbage Collection)
		 */
		else if(currentNode.previous == null) {
			currentNode = currentNode.next;
			currentNode.previous = null;
			head = currentNode;
		} 
		/*
		 * eg:-> <Head> <A> |null|1|B| --> (currentNode) <B> |A|2|C| --> <C> |B|3|null| 
		 * -----> currentNode.previous <A> |null|1|B|
		 * -----> currentNode.next	   <C> |B|3|null|
		 * 1> currentNode.previous.next = currentNode.next;
		 * i.e. <A> |null|1|C|
		 * 2> currentNode.next.previous = currentNode.previous
		 * i.e. <C> |A|3|null|
		 */
		else {
			currentNode.previous.next = currentNode.next;
			currentNode.next.previous = currentNode.previous;
		}
		
	}
	
	public static void main(String[] args) {
		DoublyLinkedList dll = new DoublyLinkedList();
		System.out.println("INSERT AT BEGIN/FRONT");
		dll.insertAtBegin(1);
		dll.insertAtBegin(10);
		dll.insertAtBegin(100);

		dll.printNode();

		System.out.println("Size of List :" + dll.size());
		System.out.println("************************************");
		System.out.println("INSERT AT REAR/END");
		dll.insertAtEnd(9);
		dll.insertAtEnd(99);

		dll.printNode();

		System.out.println("Size of List :" + dll.size());
		System.out.println("************************************");
		System.out.println("Removed First Element :" + dll.removeFirst().getData());
		dll.printNode();
		System.out.println("************************************");
		System.out.println("Removed Last Element :" + dll.removeEndApproach1().getData());
		dll.printNode();
		System.out.println("************************************");
		dll.insertAt(15, 1);
		dll.printNode();
		System.out.println("************************************");
		dll.removeAt(4);
		dll.printNode();
		System.out.println("************************************");
	}

}
