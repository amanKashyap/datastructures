package hashMap;

import java.util.HashMap;

public class Employee {
	String name;
	int age;
	
	public Employee(String name, int age) {
		this.name = name;
		this.age = age;
	}

	
	@Override
	public boolean equals(Object obj) {
		/*if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (age != other.age)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
		*/
		
		return false;
	}


	@Override
	public int hashCode() {
		/*final int prime = 31;
		int result = 1;
		result = prime * result + age;
		result = prime * result + ((name == null) ? 0 : name.hashCode());*/
		return 1;
	}
	
	public static void main(String[] args) {
		Employee e1 = new Employee("A",10);
		Employee e2 = new Employee("A",10);
		Employee e3 = new Employee("AB",10);
		Employee e4 = new Employee("A",40);	
		Employee e5 = new Employee("A",10);
		
		HashMap<Employee, String> hm = new HashMap<>();
		hm.put(e1, "v1");
		hm.put(e2, "v2");
		hm.put(e3, "v3");
		hm.put(e4, "v4");
		
		/*
		 * Firstly the get() would check if the object is same or not.
		 * using the == operator.
		 * In the or condition the content would get checked.
		 *  if (first.hash == hash && 	// always check first node
                ((k = first.key) == key // First the Object is checked via == operator. 
                || (key != null && key.equals(k))))
		 */
		System.out.println("E1 " + hm.get(e1));
		System.out.println("E2 " + hm.get(e2));
		System.out.println("E3 " + hm.get(e3));
		System.out.println("E4 " + hm.get(e4));
		
		System.out.println("E5 " + hm.get(e5));
	}
}
