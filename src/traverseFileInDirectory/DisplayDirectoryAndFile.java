/*
 * Ref:
 * https://www.mkyong.com/java/how-to-traverse-a-directory-structure-in-java/
 * https://stackoverflow.com/questions/3154488/how-do-i-iterate-through-the-files-in-a-directory-in-java
 * https://www.logicbig.com/how-to/java-io/java-iterate-dir-subdir.html
 */

package traverseFileInDirectory;

import java.io.File;
import java.util.function.Consumer;

public class DisplayDirectoryAndFile{

	public static void main (String args[]) {

		//Approach 1
		System.out.println("Prints Directories and the Files");
		displayIt(new File("C:\\Users\\aman.kashyap\\Documents\\Try"));
		System.out.println("*************************************************************");
		//Approach 2
		System.out.println("Print on the files present in the provide Directory and the SubDirectory.");
		File file = new File("C:\\Users\\aman.kashyap\\Documents\\Try");
	    fetchFiles(file, f -> System.out.println(f.getAbsolutePath()));
	}
	
	/*
	 * Using Java 7.
	 * Diff is printing all the Files first and then going to subDirectory.
	 * Printing Directories as well as the files in it.
	 */
	public static void displayIt(File node){
		//Prints the absolute Path of the Directory/File.
		System.out.println(node.getAbsoluteFile());
		if(node.isDirectory()){
			//Provide the Details of the File and Sub-Directories present in the current directory.
			String[] subNote = node.list();
			if (subNote != null) {
				for (String filename : subNote) {
					displayIt(new File(node, filename));
				}
			}
		}
		
	}
	
	public static void fetchFiles(File dir, Consumer<File> fileConsumer) {
		  //If dir found.
	      if (dir.isDirectory()) {
	          //Iterate all the files in the Directory.
	    	  for (File file1 : dir.listFiles()) {
	              //Recursively call the files in the dir to check if there is any sub-directory present or not.
	    		  fetchFiles(file1, fileConsumer);
	          }
	      } else {
	          //If No sub-directory present then print all the file names.
	    	  fileConsumer.accept(dir);
	      }
	  }
	
	
}