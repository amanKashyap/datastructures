package serializationWithInheritanceDemo;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;


class Parent {
	String name;
	
	//public Parent() {}
	
	Parent(String name) {
		this.name = name;
	}
}

public class Child extends Parent implements Serializable {

	private static final long serialVersionUID = -3395364128528076297L;
	String education;
	
	/*Child(String name) {
		super(name);
		this.education = "M.E.";
	}*/
	
	public Child() {
		super("ABCD");
		this.education = "B.E.";
	}
	
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		
		Child child2 = new Child();
		System.out.println("Child2 Name : " + child2.name);
		System.out.println("Child2 Education : " + child2.education);
		
		FileOutputStream fos = new FileOutputStream("abc.ser"); 
        ObjectOutputStream oos = new ObjectOutputStream(fos); 
              
        // Method for serialization of B's class object 
        oos.writeObject(child2); 
              
        // closing streams 
        oos.close(); 
        fos.close(); 
              
        System.out.println("Object's has been serialized"); 
          
        /* De-Serializing Child's(subclass) object */
          
        // Reading the object from a file 
        FileInputStream fis = new FileInputStream("abc.ser"); 
        ObjectInputStream ois = new ObjectInputStream(fis); 
              
        // Method for de-serialization of B's class object 
        /*
         * While DeSerializing, an exception would be raised.
         *  java.io.InvalidClassException: serializationWithInheritanceDemo.Child; 
         *  no valid constructor
         *  
         *  Because there is no no-arg constructor present in Parent Class.
         *  
         *  If We add a No-Arg constructor, DeSerialization would occur perfectly.
         *  Also, name field of Parent class would have null value.
         *  As Parent class doesn't implement Serializable Interface.
         */
        Child child2DeSer = (Child)ois.readObject(); 
              
        // closing streams 
        ois.close(); 
        fis.close(); 
              
        System.out.println("Object has been deserialized"); 
		System.out.println("Child2DeSer Name : " + child2DeSer.name);
		System.out.println("Child2DeSer Education : " + child2DeSer.education);
	}
}

        