package customSinglyLinkedList;

/*
 * Remove Duplicates from a Sorted Singly Linked List.
 */
public class RemoveDuplicatesFromSinglyLinkedList {

	private ListNode head; // HeadNode

	// Contains a Static Inner Class ListNode -- Node (data|next)
	private static class ListNode {
		private int data;
		private ListNode next;

		public ListNode(int data) {
			this.data = data;
			this.next = null;
		}
	}

	public void printLinkedList() {
		if (head == null) {
			System.out.println("Linked List is Empty");
		}
		ListNode current = head;
		while (null != current) {
			System.out.print(current.data + "--> ");
			current = current.next;
		}
		System.out.print("null");
		System.out.println();
	}

	
	public void removeDuplicates() {
		/*
		 * To check if the LinkedList is Empty or not. Check the boundary condition.
		 */
		if (head == null) {
			System.out.println("The Linked List is empty");
			return;
		}
		
		ListNode currentNode = head;
		while(currentNode != null && currentNode.next != null) {
			/*
			 * As it is a Sorted Linked List.
			 * If the currentNode's data is equal to the Next Node Data.
			 * We need to remove the next node connection.
			 * 
			 * As there would be no reference left of the currentNode next node.
			 * It would be garbage collected.
			 */
			if(currentNode.data == currentNode.next.data) {
				currentNode.next = currentNode.next.next;
			} else {
				currentNode = currentNode.next;
			}
		}
	}
	
	public static void main(String[] args) {

		RemoveDuplicatesFromSinglyLinkedList singlyLinkedList = new RemoveDuplicatesFromSinglyLinkedList();
		/*
		 * Create Linked List Demo 1-->1-->2-->2-->3-->3--null 
		 * 1 -- headNode
		 */
		singlyLinkedList.head = new ListNode(1);
		ListNode second = new ListNode(1);
		ListNode third = new ListNode(1);
		ListNode fourth = new ListNode(2);
		ListNode five = new ListNode(3);
		ListNode sixth = new ListNode(3);

		// Attach all the nodes to form a List.
		singlyLinkedList.head.next = second;
		second.next = third;
		third.next = fourth;
		fourth.next = five;
		five.next = sixth;

		singlyLinkedList.printLinkedList();
		System.out.println("**************************************");
		singlyLinkedList.removeDuplicates();
		singlyLinkedList.printLinkedList();
	}
}
