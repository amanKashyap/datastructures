package customSinglyLinkedList;

public class SinglyLinkedList {

	/*
	 * Kind of a pointer, pointing to the head Node i.e. head ListNode.
	 * It holds the complete LinkedList.
	 * The only point for Iteration in Linked List
	 */
	private ListNode head; //HeadNode
	
	//Contains a Static Inner Class ListNode -- Node (data|next)
	private static class ListNode {
		private int data;
		private ListNode next;
		
		public ListNode(int data) {
			this.data = data;
			this.next = null;
		}
	}
	
	/*
	 * Given a LinkedList, Print all the elements it holds.
	 * No Need to provide the Entire LinkedList, 
	 * Only by passing HeadNode we can traverse the List
	 */
	public void display(ListNode head) {
		//Check If LinkedList is null or not. I.e. Empty
		if(head == null) {
			return;
		}
		ListNode currentNode = head;
		/*
		 * Loop Each Element till we reach the end of List.
		 * Last Node always points to null.
		 */
		while(currentNode != null) {
			//Print Current Node Data
			System.out.print(currentNode.data + "-->");
			//Move to Next Node.
			currentNode = currentNode.next;
		}
		//Here Current Node would be null as it has reached end of the List.
		System.out.print(currentNode);
		System.out.println();
	}
	
	//Given ListNode head, find length of the Linked List
	public int length(ListNode head) {
		//Check If LinkedList is empty or not.
		
		if(head == null) {
			return 0;
		}
		//Count to hold length of the LinkedList
		int count = 0;
		//currentNode is used to hold the current Node of the LinkedList.
		ListNode currentNode = head;
		/*
		 * Loop Each Element till we reach the end of List.
		 * Last Node always points to null.
		 * While Iterating increment the value of Count.
		 */
		while(currentNode != null) {
			//Increment the value of count.
			count++;
			//Move to Next Node.
			currentNode = currentNode.next;
		}
		return count;
	}

	
	/*
	 * Used to Insert a newNode at the beginning of the LinkedList.
	 * @Arg - 1. HeadNode
	 * 		  2. Data of the NewNode
	 * @Return - Reference to new HeadNode i.e. Returns new HeadNode.
	 */
	public ListNode insertAtBeginning(ListNode head, int data) {
		//Create a New Node.
		ListNode newNode = new ListNode(data);
		/*
		 * If Head is null.
		 * Then the LinkedList is empty and we can return directly the newNode.
		 */
		if(head == null) {
			return newNode;
		}
		
		//Connect the newNode to the start of the HeadNode.
		newNode.next = head;
		//Make the NewNode as the HeadNode.
		head = newNode;
		/*
		 * Return the newly added HeadNode.
		 * Which is actually the newNode.
		 */
		return head;	
	}
	
	
	/*
	 * Used to Insert a newNode at the end of the LinkedList.
	 * @Arg - 1. HeadNode
	 * 		  2. Data of the NewNode
	 * @Return - Reference to headNode
	 */
	public ListNode insertAtEnd(ListNode head, int data) {
		//Create a New Node.
		ListNode newNode = new ListNode(data);
		/*
		 * If Head is null.
		 * Then the LinkedList is empty and we can return directly the newNode.
		 */
		if(head == null) {
			return newNode;
		}
		//currentNode holds the Current Node of the LinkedList.
		ListNode currentNode = head;
		/*
		 * Loop Each Element till we reach last element.
		 */
		while(null != currentNode.next) {
			//Move to Next Node.
			currentNode = currentNode.next;
		}
		/*
		 * Connected the current LastNode to the newNode.
		 * Making the newNode as the Last Node inserted into the LinkedList.
		 */
		currentNode.next = newNode;
		
		return head;	
	}
	
	/*
	 * Given a Node (previousNode) and data.
	 * Add a newNode after the previousNode with the specified data.
	 */
	public void insertAfter(ListNode previousNode , int data) {
		
		if(previousNode == null) {
			System.out.println("The Given Previous Node cannot be Null");
			return;
		}
		
		ListNode newNode = new ListNode(data);
		newNode.next = previousNode.next;
		previousNode.next = newNode;
	}
	
	
	/*
	 * Given Head Node, Int data and the int position.
	 * Insert data to as a Node to the specified location.
	 */
	public ListNode insertAtPosition(ListNode head, int data, int position) {
		//Perform Boundary Condition check
		int size = length(head);
		if(position > size + 1 || position < 1) {
			System.out.println("Invalid Position.");
			return head;
		}
		
		ListNode newNode = new ListNode(data);
		
		if(position == 1) {
			newNode.next = head;
			return head;
		} else {
			/*
			 * Temporary Variable previous to store head.
			 * And to Iterate from head to the desired position.
			 */
			ListNode previous = head;
			/*
			 * Used to track traversal of the previous node.
			 */
			int count = 1;
			/*
			 * While loop is used to traverse to the required position - 1 Node.
			 * So that a newNode can be added to the given position.
			 */
			while(count < position -1) {
				previous = previous.next;
				count++;
			}
			/*
			 * Used to create a connection between the newNode and the next node.
			 * Which is currently stored at previous.next.
			 * Create a connection:
			 * previous --> newNode --> currentNode
			 */
			ListNode current = previous.next;
			newNode.next = current;
			previous.next = newNode;
			/*
			 * HeadNode hold he LinkedList with the inserted Node.
			 */
			return head;
		}
	}
	
	
	/*
	 * Given ListNode head.
	 * Delete the firstNode from the Linked List.
	 */
	public ListNode deleteFirst(ListNode head) {
		if(head == null) {
			//Usually an Exception is thrown.
			System.out.println("No Node available in LinkedList");
			return head;
		}
		
		/*
		 * Temp variable to store the head node.
		 * And return the headNode when the pointer has moved to the nextNode.
		 * next Pointer is set to null.
		 * For disconnecting it with the LinkedList.
		 */
		ListNode temp = head;
		head = head.next;
		temp.next = null;
		
		return temp;
	}
	
	
	/*
	 * Given ListNode head.
	 * Delete the lastNode from the Linked List.
	 */
	public ListNode deleteLast(ListNode head) {
		if(head == null) {
			//Usually an Exception is thrown.
			System.out.println("No Node available in LinkedList");
			return head;
		}
		
		ListNode last = head;
		ListNode previousToLast = null;
		
		/*
		 * Iterate the Linked List.
		 * Till the last points to the Last Node of the LinkedList.
		 */
		while(last.next != null) {
			previousToLast = last;
			last = last.next;
		}
		
		//Disconnect the previouToLast node from the Last Node.
		previousToLast.next = null;
		return last;
	}
	
	
	/*
	 * Given ListNode head and int position.
	 * Delete Node at the specified Location.
	 */
	public ListNode deleteAtPosition(ListNode head, int position) {
		
		//To check the Boundary Condition.
		int size = length(head);
		if(position > size || position < 1) {
			//Usually a exception would be raised.
			System.out.println("Position is not in Range. Invalid position");
			return head;
		}
		
		//If we need to delete the FirstNode.
		if(position == 1) {
			ListNode temp = head;
			head = head.next;
			temp.next = null;
			return temp;
		} else {
			ListNode previous = head;
			int count = 1;
			
			while(count < position-1) {
				previous = previous.next;
				count++;
			}
			
			ListNode current = previous.next;
			previous.next = current.next;
			current.next = null;
			
			return current;
		}
	}
	
	
	public boolean searchKey(ListNode head, int searchKey) {
		//If the LinkedList is empty. Return false.
		if(head == null) {
			return false;
		}
		
		ListNode current = head;
		while(current != null) {
			if(current.data == searchKey) {
				return true;
			}
			current = current.next;
		}
		
		return false;
	}
	
	
	public static void main(String[] args) {
		/*
		 * Create Linked List Demo
		 * 10-->8-->1--null
		 * 10 -- headNode
		 */
		ListNode head = new ListNode(10);
		ListNode second = new ListNode(8);
		ListNode third = new ListNode(1);
		
		//Attach all the nodes to form a List.
		head.next = second; // 10-->8
		second.next = third; // 10-->8-->1-->null
		
		SinglyLinkedList singlyLinkedList = new SinglyLinkedList();
		/*
		 * Display the LinkedList -- display(Node Head)
		 */
		singlyLinkedList.display(head);
		System.out.println();
		System.out.println("******************************");
		/*
		 * Length of  the LinkedList -- length(Node Head)
		 */
		System.out.println("Length of Linked List : "+ singlyLinkedList.length(head));
		/*
		 * Insert Node at the begining of  the LinkedList -- insertAtBeginning(Node Head, int data)
		 */
		ListNode newHeadInsertAtBegin = singlyLinkedList.insertAtBeginning(head, 15);
		System.out.println("INSERT AT THE BEGINNING OF THE LINKED LIST");
		singlyLinkedList.display(newHeadInsertAtBegin);
		/*
		 * Insert Node at the end of  the LinkedList -- insertAtEnd(Node Head, int data)
		 */
		System.out.println();
		
		System.out.println("******************************");
		ListNode newHeadInsertAtEnd = singlyLinkedList.insertAtEnd(head, 25);
		System.out.println("INSERT AT THE END OF THE LINKED LIST");
		singlyLinkedList.display(newHeadInsertAtEnd);
		/*
		 * Insert Node after a given previousNode -- insertAfter(Node previous, int data)
		 */
		
		System.out.println();
		System.out.println("******************************");
		singlyLinkedList.insertAfter(second, 35);
		System.out.println("INSERT AFTER A GIVEN NODE");
		singlyLinkedList.display(head);
		System.out.println("******************************");
		/*
		 * Insert Node after a given position -- 
		 * -- insertAtPosition(Node head, int data, int position)
		 */
		ListNode newHead = singlyLinkedList.insertAtPosition(head, 155, 4);
		System.out.println("INSERT AT A GIVEN POSITION");
		singlyLinkedList.display(newHead);
		System.out.println("******************************");
		
		/*
		 * Delete first Node -- deleteFirst(Node head)
		 */
		singlyLinkedList.display(head);
		System.out.println("******************************");
		System.out.println("DELETE FIRST NODE OF THE LINKED LIST");
		ListNode firstNode = singlyLinkedList.deleteFirst(head);
		System.out.println(firstNode.data);
		System.out.println("******************************");
		/*
		 * Delete last Node -- deleteLast(Node head)
		 */
		singlyLinkedList.display(second);
		System.out.println("******************************");
		System.out.println("DELETE LAST NODE OF THE LINKED LIST");
		ListNode last = singlyLinkedList.deleteLast(second);
		System.out.println(last.data);
		System.out.println("******************************");
		/*
		 * Delete Node at a Given Position -- deleteAtPosition(Node head, int position)
		 */
		singlyLinkedList.display(second);
		System.out.println("******************************");
		System.out.println("DELETE NODE AT GIVEN POSTION OF THE LINKED LIST");
		ListNode deletedNode = singlyLinkedList.deleteAtPosition(second, 3);
		System.out.println(deletedNode.data);
		System.out.println("******************************");
		
		singlyLinkedList.display(second);
		System.out.println("******************************");
		System.out.println("SEARCH NODE HAVING DATA as 35");
		boolean found = singlyLinkedList.searchKey(second, 35);
		if(found) {
			System.out.println("SEARCH KEY FOUND");
		} else {
			System.out.println("SEARCH KEY NOT FOUND");
		}
		System.out.println("******************************");
	}
	
}
