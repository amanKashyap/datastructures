package customSinglyLinkedList;

public class DetectLoopInList {

	private ListNode head; // HeadNode

	// Contains a Static Inner Class ListNode -- Node (data|next)
	private static class ListNode {
		private int data;
		private ListNode next;

		public ListNode(int data) {
			this.data = data;
			this.next = null;
		}
	}

	public void printLinkedList() {
		if (head == null) {
			System.out.println("Linked List is Empty");
		}
		ListNode current = head;
		while (null != current) {
			System.out.print(current.data + "--> ");
			current = current.next;
		}
		System.out.print("null");
		System.out.println();
	}

	public boolean çontainsLoop() {
		ListNode fastPointer = head;
		ListNode slowPointer = head;
		
		while(fastPointer != null && fastPointer.next != null) {
			//Traverse two nodes at a time.
			fastPointer = fastPointer.next.next;
			//Traverse One Node at a time.
			slowPointer = slowPointer.next;
			
			if(slowPointer == fastPointer) {
				System.out.println("Loop Detected");
				return true;
			}
		}
		return false;
	}
	
	/*
	 * Used to create a Loop for the Linked List.
	 */
	public void createAtLoopInLinkedList() {
		
		ListNode first = new ListNode(1);
		ListNode second = new ListNode(2);
		ListNode third = new ListNode(3);
		ListNode fourth = new ListNode(4);
		ListNode five = new ListNode(5);
		ListNode sixth = new ListNode(6);
		
		head = first;
		first.next = second; // 1-->2
		second.next = third; // 1-->2-->3
		third.next = fourth; // 1-->2-->3-->4-->null
		fourth.next = five; // 1-->2-->3-->4-->5-->null
		five.next = sixth; // 1-->2-->3-->4-->5-->6-->null
		sixth.next = third; // 1-->2-->3-->4-->5-->6-->3 (LOOP CREATED)
	}
	
	public static void main(String[] args) {
		DetectLoopInList singlyLinkedList = new DetectLoopInList();
		singlyLinkedList.createAtLoopInLinkedList();
		singlyLinkedList.çontainsLoop();
	}
}
