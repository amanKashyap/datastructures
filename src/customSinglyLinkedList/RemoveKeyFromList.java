package customSinglyLinkedList;

public class RemoveKeyFromList {

	private ListNode head; // HeadNode

	// Contains a Static Inner Class ListNode -- Node (data|next)
	private static class ListNode {
		private int data;
		private ListNode next;

		public ListNode(int data) {
			this.data = data;
			this.next = null;
		}
	}

	public void printLinkedList() {
		if (head == null) {
			System.out.println("Linked List is Empty");
		}
		ListNode current = head;
		while (null != current) {
			System.out.print(current.data + "--> ");
			current = current.next;
		}
		System.out.print("null");
		System.out.println();
	}

	public void deleteNode(int key) {
		ListNode currentNode = head;
		ListNode tempNode = null;
		
		/*
		 * If headNode contains the Key. i.e. head.data == key 
		 */
		while(currentNode != null && currentNode.data == key) {
			head = currentNode.next;
			return ;
		}
		
		/*
		 * To check for the remaining Nodes.
		 */
		while(currentNode != null && currentNode.data != key) {
			tempNode = currentNode;
			currentNode = currentNode.next;
		}
		
		/*
		 * If End of the List is reached.
		 * And the Key is not found.
		 */
		if(currentNode == null) {
			System.out.println("Key Not Found");
			return ;
		}
		
		/*
		 * Remove the reference of the currentNode.
		 * So that It can be garbage collected.
		 */
		tempNode.next = currentNode.next;
	}
	
	public static void main(String[] args) {

		RemoveKeyFromList singlyLinkedList = new RemoveKeyFromList();
		/*
		 * Create Linked List Demo 10-->8-->1-->11--null 10 -- headNode
		 */
		singlyLinkedList.head = new ListNode(1);
		ListNode second = new ListNode(8);
		ListNode third = new ListNode(10);
		ListNode fourth = new ListNode(11);
		ListNode five = new ListNode(15);

		// Attach all the nodes to form a List.
		singlyLinkedList.head.next = second; // 10-->8
		second.next = third; // 10-->8-->1
		third.next = fourth; // 10-->8-->1-->11-->null
		fourth.next = five;  // 10-->8-->1-->11-->15-->null

		singlyLinkedList.printLinkedList();
		System.out.println("**************************************");
		singlyLinkedList.deleteNode(1);
		singlyLinkedList.printLinkedList();
	}
}
