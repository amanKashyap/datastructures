package customSinglyLinkedList;

public class FindMiddleNodeSinglyLinkedList {

	private ListNode head; // HeadNode

	// Contains a Static Inner Class ListNode -- Node (data|next)
	private static class ListNode {
		private int data;
		private ListNode next;

		public ListNode(int data) {
			this.data = data;
			this.next = null;
		}
	}

	public void printLinkedList() {
		if (head == null) {
			System.out.println("Linked List is Empty");
		}
		ListNode current = head;
		while (null != current) {
			System.out.print(current.data + "--> ");
			current = current.next;
		}
		System.out.print("null");
		System.out.println();
	}

	/*
	 * Find out the middle node of the Singly Linked List.
	 */
	public ListNode getMiddleNode() {
		if (head == null) {
			System.out.println("Linked List is Empty");
		}

		ListNode slowPointer = head;
		ListNode fastPointer = head;

		while (fastPointer != null && fastPointer.next != null) {
			slowPointer = slowPointer.next;
			fastPointer = fastPointer.next.next;
		}

		return slowPointer;
	}

	public static void main(String[] args) {
		
		FindMiddleNodeSinglyLinkedList singlyLinkedList = new FindMiddleNodeSinglyLinkedList();
		/*
		 * Create Linked List Demo 10-->8-->1-->11--null 10 -- headNode
		 */
		singlyLinkedList.head = new ListNode(10);
		ListNode second = new ListNode(8);
		ListNode third = new ListNode(1);
		ListNode fourth = new ListNode(11);
		ListNode five = new ListNode(15);

		// Attach all the nodes to form a List.
		singlyLinkedList.head.next = second; // 10-->8
		second.next = third; // 10-->8-->1
		third.next = fourth; // 10-->8-->1-->11-->null
		fourth.next = five;

		
		singlyLinkedList.printLinkedList();
		System.out.println("**************************************");
		System.out.println("Middle Node Data is : " + singlyLinkedList.getMiddleNode().data);
	}
}
