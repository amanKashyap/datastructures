package customSinglyLinkedList;

public class ReverseSinglyLinkedList {
	
		private ListNode head; //HeadNode
		
		//Contains a Static Inner Class ListNode -- Node (data|next)
		private static class ListNode {
			private int data;
			private ListNode next;
			
			public ListNode(int data) {
				this.data = data;
				this.next = null;
			}
		}
		
		/* 
		 * Given a LinkedList, Print all the elements it holds.
		 * No Need to provide the Entire LinkedList, 
		 * Only by passing HeadNode we can traverse the List
		 */
		public void display(ListNode head) {
			//Check If LinkedList is null or not. I.e. Empty
			if(head == null) {
				return;
			}
			ListNode currentNode = head;
			/*
			 * Loop Each Element till we reach the end of List.
			 * Last Node always points to null.
			 */
			while(currentNode != null) {
				//Print Current Node Data
				System.out.print(currentNode.data + "-->");
				//Move to Next Node.
				currentNode = currentNode.next;
			}
			//Here Current Node would be null as it has reached end of the List.
			System.out.print(currentNode);
			System.out.println();
		}
		
		
		/*
		 * Given the Head Node of the Singly Linked List.
		 * Reverse the Linked List.
		 * Return newHead Node.
		 */
		public ListNode reverse(ListNode head) {
			if(head == null) {
				System.out.println("Linked List is empty");
				return head;
			}
			
			ListNode current = head;
			ListNode previous = null;
			ListNode next = null;
			
			while(current != null) {
				next = current.next;
				current.next = previous;
				previous = current;
				current = next;
			}
			
			return previous;
		}
		
		public static void main(String[] args) {
			/*
			 * Create Linked List Demo
			 * 10-->8-->1-->11--null
			 * 10 -- headNode
			 */
			ListNode head = new ListNode(10);
			ListNode second = new ListNode(8);
			ListNode third = new ListNode(1);
			ListNode fourth = new ListNode(11);
			
			//Attach all the nodes to form a List.
			head.next = second; // 10-->8
			second.next = third; // 10-->8-->1
			third.next = fourth; // 10-->8-->1-->11-->null
			
			ReverseSinglyLinkedList singlyLinkedList = new ReverseSinglyLinkedList();
			singlyLinkedList.display(head);
			
			System.out.println("********************************************");
			System.out.println("Reverse a Given Linked List");
			ListNode reversedListHead = singlyLinkedList.reverse(head);
			System.out.println("********************************************");
			singlyLinkedList.display(reversedListHead);
		}
}
