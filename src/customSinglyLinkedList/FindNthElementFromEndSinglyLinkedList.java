package customSinglyLinkedList;

/*
 * Given the headNode and integer n.
 * Find the nth Node from the end of a Singly Linked List.
 */
public class FindNthElementFromEndSinglyLinkedList {

	private ListNode head; // HeadNode

	// Contains a Static Inner Class ListNode -- Node (data|next)
	private static class ListNode {
		private int data;
		private ListNode next;

		public ListNode(int data) {
			this.data = data;
			this.next = null;
		}
	}

	public void printLinkedList() {
		if (head == null) {
			System.out.println("Linked List is Empty");
		}
		ListNode current = head;
		while (null != current) {
			System.out.print(current.data + "--> ");
			current = current.next;
		}
		System.out.print("null");
		System.out.println();
	}

	public ListNode getNthNodeFromEnd(int n) {
		/*
		 * To check if the LinkedList is Empty or not. Check the boundary condition.
		 */
		if (head == null) {
			System.out.println("The Linked List is empty");
			return null;
		}
		/*
		 * To check if the value of n provided is valid or not. Check the boundary
		 * condition.
		 */
		if (n <= 0) {
			System.out.println("Invalid value of N");
			throw new IllegalArgumentException("Invalid Value: n = " + n);
		}

		ListNode mainPointer = head;
		ListNode refPointer = head;
		int count = 0;

		/*
		 * Idea is to create the difference between position of refPointer and
		 * mainPointer by 'n' nodes.
		 */
		while (count < n) {
			/*
			 * Checking the value of n against the Number of Nodes. N should not be greater
			 * than the Number of Nodes in the Linked List.
			 */
			if (refPointer == null) {
				throw new IllegalArgumentException(n + " is greater than the Number of Nodes in the List");
			}
			refPointer = refPointer.next;
			count++;
		}

		/*
		 * As the difference between position of refPointer and mainPointer is n nodes.
		 * Hence, Iterate the refPointer till it reaches null. At that point the
		 * mainPointer would be pointing to the nth Node from the end.
		 */
		while (refPointer != null) {
			refPointer = refPointer.next;
			mainPointer = mainPointer.next;
		}

		return mainPointer;
	}

	public static void main(String[] args) {

		FindNthElementFromEndSinglyLinkedList singlyLinkedList = new FindNthElementFromEndSinglyLinkedList();
		/*
		 * Create Linked List Demo 10-->8-->1-->11--null 10 -- headNode
		 */
		singlyLinkedList.head = new ListNode(10);
		ListNode second = new ListNode(8);
		ListNode third = new ListNode(1);
		ListNode fourth = new ListNode(11);
		ListNode five = new ListNode(15);

		// Attach all the nodes to form a List.
		singlyLinkedList.head.next = second; // 10-->8
		second.next = third; // 10-->8-->1
		third.next = fourth; // 10-->8-->1-->11-->null
		fourth.next = five;

		singlyLinkedList.printLinkedList();
		System.out.println("**************************************");
		System.out.println("Nth Node from End is : " + singlyLinkedList.getNthNodeFromEnd(2).data);
	}
}
