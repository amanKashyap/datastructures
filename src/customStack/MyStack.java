package customStack;

import javax.swing.plaf.synth.SynthSeparatorUI;

public class MyStack {
	
	private int maxSize;
	private int top;
	private String arr[];
	
	/*
	 * Take the max Size as an argument, at the time of initializing the Stack.
	 * Instantiate a New Array of String of maxSize to hold the data.
	 * Top would act a pointer to the location at which we need to store the data.
	 */
	public MyStack(int size) {
		this.maxSize = size;
		arr= new String[this.maxSize];
		top = 0;
	}
	
	/*
	 * Used to check if the Stack is empty or not.
	 */
	public boolean empty() {
		if (top == 0) {
			return true;
		}
		return false;
	}
	
	/*
	 * Used to check if the Stack is full or not.
	 */
	public boolean stackFull() {
		if(top < maxSize) {
			return false;
		}
		return true;
	}
	
	/*
	 * Logic to add an element into the Stack.
	 * First we will check if the Stack is full or not.
	 * We need to add the element into the Stack and Increase the top counter.
	 */
	public void push(String str) {
		if (top < maxSize) {
			arr[top] = str;
			top++;
		} else {
			System.out.println("Stack if Full.. Stack OverFlow !!!");
			return;
		}
	}
	
	/*
	 * This method can be used to return the last element entered into the Stack.
	 * First, We need to check if the Stack is empty of not.
	 */
	public String peek() {
		if (!this.empty()) {
			return arr[top - 1];
		} else {
			System.out.println("Stack is Empty!!");
			return null;
		}
	}
	
	/*
	 * This method is used to return and remove the last entered element from the Stack.
	 * First, We need to check if the Stack is empty of not.
	 * After removing the element, we need to decrement the top counter.
	 */
	public String pop() {
		if (!this.empty()) {
			// String temp = this.peek();
			String temp = arr[top - 1];
			arr[top - 1] = null;
			top--;

			return temp;
		} else {
			System.out.println("Stack is Empty!!");
			return null;
		}
	}
	
	/*
	 * Used to display all the elements of the Stack.
	 */
	public void display() {
		if(!this.empty()) {
			for(int temp = top-1; temp >= 0; temp--) {
				System.out.println("Element : "+ arr[temp]);
			}
		} else {
			System.out.println("Stack is Empty !!!");
		}
	}
}
