package customStack;

public class Main {
	public static void main(String[] args) {
		MyStack stack = new MyStack(5);
		System.out.println("****************************************");
		System.out.println("Stack Empty : "+ stack.empty());
		stack.push("A");
		stack.push("B");
		stack.push("C");
		stack.push("D");
		System.out.println("Stack Pop : " + stack.pop());
		System.out.println("Stack Peek : " + stack.peek());
		System.out.println("****************************************");
		stack.push("E");
		stack.push("F");
		System.out.println("****************************************");
		System.out.println("Stack Full : " + stack.stackFull());
		System.out.println("****************************************");
		stack.display();
	}
}
