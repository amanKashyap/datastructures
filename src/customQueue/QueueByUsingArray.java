package customQueue;

/*
 * Queue's follow FIFO approach.
 * It has two pointers Front and Rear.
 * Enqueue - Operation of adding an element in the queue.
 * Dequeue - Operation of removing a queue.
 * 
 * A queue of Integer values
 */
public class QueueByUsingArray {

	int front, rear;
	//An array to hold the value of the queue.
	int arr[];
	//Holds the Size of the array.
	int size;
	
	public QueueByUsingArray(int size) {
		this.size = size;
		arr = new int[size];
		front = -1;
		rear = -1;
	}
	
	public boolean isEmpty() {
		if (front == -1 && rear == -1) {
			return true;
		}
		return false;
	}
	
	public boolean isFull() {
		if (rear == size) {
			return true;
		}
		return false;
	}
	
	public void enqueue(int data) {
		if (isFull()) {
			System.out.println("Queue is Full !!!");
			return;
		} else if (isEmpty()) {
			front = 0;
			rear = 0;
			arr[rear] = data;
		} else {
			rear++;
			arr[rear] = data;
		}
	}
	
	
	public int dequeue() {
		int temp = 0;
		if (isEmpty()) {
			System.out.println("Queue is Empty !!!");
		} else if (front == rear) {
			temp = arr[front];
			System.out.println("Deleting the last element left from the Queue !!!");
			front = -1;
			rear = -1;
		} else {
			temp = arr[front];
			front++;
		}
		return temp;
	}
	
	public void display() {
		if (isEmpty()) {
			System.out.println("Queue is Empty!!!!!");
		} else {
			for (int i = front; i < rear; i++) {
				System.out.println(arr[i]);
			}
		}
	}
	
	/*
	 * We can not add 2 inside the Queue even when we have performed dequeue operation.
	 * This is a demerit of using a array.
	 * As when we increment front counter during we dequeue.
	 * We gradually decrease the spaces available.
	 * 
	 * This problem can be solved using a circular Array.
	 * In which we can use a index again and again.
	 */
	public static void main(String[] args) {
		QueueByUsingArray queue = new QueueByUsingArray(10);
		System.out.println("Is Queue Empty : " + queue.isEmpty());
		System.out.println("**********************");
		queue.enqueue(5);
		queue.enqueue(6);
		queue.enqueue(7);
		queue.display();
		System.out.println("**********************");
		System.out.println("Dequeue : " + queue.dequeue());
		queue.enqueue(8);
		queue.enqueue(9);
		queue.enqueue(10);
		queue.enqueue(4);
		queue.enqueue(6);
		System.out.println("Dequeue : " + queue.dequeue());
		queue.enqueue(11);
		queue.enqueue(12);
		System.out.println("Dequeue : " + queue.dequeue());
		System.out.println("Dequeue : " + queue.dequeue());
		System.out.println("Dequeue : " + queue.dequeue());
		queue.enqueue(2);
	}
}
