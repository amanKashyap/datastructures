package customQueue;

public class QueueByUsingCircularArray {

	int front, rear;
	//An array to hold the value of the queue.
	int arr[];
	//Holds the Size of the array.
	int size;
	
	public QueueByUsingCircularArray(int size) {
		this.size = size;
		arr = new int[size];
		front = -1;
		rear = -1;
	}
	
	public boolean isEmpty() {
		if (front == -1 && rear == -1) {
			return true;
		}
		return false;
	}
	
	/*
	 * Because we are using a circular array.
	 * Hence the logic for checking if the queue is full or not would be changed:
	 * (rear + 1) % size == front
	 */
	public boolean isFull() {
		if ((rear + 1) % size == front) {
			return true;
		}
		return false;
	}
	
	public int front() {
		if (isEmpty()) {
			return 0;
		} else {
			return arr[front];
		}
	}
	
	public void enqueue(int data) {
		if (isFull()) {
			System.out.println("Queue is Full !!");
		} else if (isEmpty()) {
			System.out.println("Entering First Element into the Queue!!");
			front = 0;
			rear = 0;
			arr[rear] = data;
		} else {
			/*
			 * As this is a Circular array, we need to find the new Rear index using
			 * below mentioned logic.
			 * rear = (rear+1) % size
			 */
			rear = (rear+1) % size;
			arr[rear] = data;
		}
	}
	
	
	public int dequeue() {
		int temp = 0;
		if(isEmpty()) {
			System.out.println("Queue is Empty !!!");
		} else if (front == rear) {
			System.out.println("Emptying the queue!!!");
			temp = arr[front];
			front = -1;
			rear = -1;
		} else {
			temp = arr[front];
			front = (front + 1) % size;
		}
		return temp;
	}
	
	public void display() {
		if(front == -1 && rear == -1) {
			System.out.println("Queue is Empty !!!");
		} else {
			System.out.println("Elements of the Queue are :");
			int tempFront = front;
			for(int i = 0; i < size ; i++) {
				if(tempFront != rear) {
					System.out.println("Element : " + arr[tempFront]);
					//Calculate the Next front Index.
					tempFront = (tempFront + 1) % size;
				} else {
					System.out.println("Element : " + arr[rear]);
					System.out.println("Queue Ended !!!!");
					break;
				}
			}
		}
	}
	
	
	public static void main(String[] args) {
		QueueByUsingCircularArray queue = new QueueByUsingCircularArray(10);
		System.out.println("Is Queue Empty : " + queue.isEmpty());
		System.out.println("**********************");
		queue.enqueue(5);
		queue.enqueue(6);
		queue.enqueue(7);
		queue.display();
		System.out.println("**********************");
		//System.out.println("Dequeue : " + queue.dequeue());
		queue.enqueue(8);
		queue.enqueue(9);
		queue.enqueue(10);
		queue.enqueue(4);
		queue.enqueue(6);
		//System.out.println("Dequeue : " + queue.dequeue());
		queue.enqueue(11);
		queue.enqueue(12);
		System.out.println("Dequeue : " + queue.dequeue());
		System.out.println("Dequeue : " + queue.dequeue());
		System.out.println("Dequeue : " + queue.dequeue());
		queue.enqueue(2);
		queue.display();
	}

}
