package customQueue;

public class QueueByUsingLinkedList {
	
	private ListNode front; //Front Node
	private ListNode rear; //Rear Node
	
	//Contains a Static Inner Class ListNode -- Node (data|next)
	private static class ListNode {
		private int data;
		private ListNode next;
		
		public ListNode(int data) {
			this.data = data;
			this.next = null;
		}
	}
	
	public boolean isEmpty() {
		if (front == null && rear == null) {
			return true;
		}
		return false;
	}
	
	public void enqueue(int data) {
		ListNode tempNode = new ListNode(data);

		if (isEmpty()) {
			front = tempNode;
			rear = tempNode;
		} else {
			rear.next = tempNode;
			rear = tempNode;
		}
	}
	
	
	public ListNode dequeue() {
		ListNode tempNode = front;
		if(isEmpty()) {
			System.out.println("Queue is empty !!");
			return tempNode;
		}
		else if (front == rear) {
			System.out.println("Emptying the Queue !!!");
			front = null;
			rear = null;
			return tempNode;
		}
		else {
			front = front.next;
			return tempNode;
		}
	}
	
	
	public void display() {
		if (isEmpty()) {
			System.out.println("Queue is empty !!!!");
		} else {
			ListNode tempNode = this.front;
			while (tempNode != null) {
				System.out.println("Element in queue : " + tempNode.data);
				tempNode = tempNode.next;
			}
		}
	}
	
	public static void main(String[] args) {
		QueueByUsingLinkedList queue = new QueueByUsingLinkedList();
		queue.enqueue(1);
		queue.enqueue(2);
		queue.enqueue(3);
		queue.enqueue(4);
		queue.display();
		System.out.println("*************************************");
		ListNode dequeued = queue.dequeue();
		if (dequeued != null) {
			System.out.println("Removed Element : " + dequeued.data);
		}
		System.out.println("*************************************");
		queue.enqueue(5);
		queue.enqueue(6);
		dequeued = queue.dequeue();
		if (dequeued != null) {
			System.out.println("Removed Element : " + dequeued.data);
		}
		System.out.println("*************************************");
		queue.enqueue(7);
		queue.display();
	}
}
